ScratchBasic Interpreter version 0.5 15/05/2016

OVERVIEW
--------
This application helps users to write code using the drag and drop functionality. The application is for beginners who can write code for a ScratchBasic program. This interpreter application runs the code written by the user and displays the output on a console.

Usage
-----
- An easy-to-use drag and drop functionality has been implemented.

- A PRINT statement that outputs a value onto the console.

- A LET statement that sets a variable to a value.

- A GOTO statement that takes the program to a certain line number as mentioned by the user.

- A IF/GOTO statement that evaluates an expression and if the condition is true, it takes the          program to a certain line number as mentioned by the user.

- A REM statement that allows users to place comments in the code

- A GOSUB/RETURN statement which allows for function calling. Nested GOSUB is possible

- The values of a statement can be edited.

- Multiple statements can be deleted at the same time.

- The interpreter file can be reset, which would delete all the statements in the application.

- There is a help guide for further references

Getting Started
---------------
- When the application is started, a blank ScratchBasic program can be seen, with all the      statements on the left hand side of the screen and a console on the bottom.

- Statements can be added to the program by dragging and dropping.

- All values that are needed, have to be entered by the user in the textfields.

- The run button will run the program and output the result on the console.

- To delete one or more statements, tap and hold a statement after which that statement is
  selected. Tap and hold other statement you wish to delete and tap on the delete button on the top
  right corner to confirm.

- To view the help guide, tap on the help button on the menu bar

Limitations
-----------

- None

Developers
----------
Joshua Simon Thomas (26515598)
jstho7@student.monash.edu
Arham Imran Abbasi (26354195)
aiaab1@student.monash.edu