package au.edu.monash.fit3140.asgn3.android_interpreter;

import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.ActionMode;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;


import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/*
 * This class is main activity from which the application starts from. It initializes all the GUI components that
 * are required sets up the app's functionality like drag and drop, line selection, and executing the file
 *
 */


public class MainActivity extends AppCompatActivity {

    private ListView instList;
    private FrameLayout frameListLayout;
    private TextView consoleTextView;

    private ArrayList<Instructions> instlines;
    private DataArrayAdapter listAdapter;
    //private Interpreter it;
    private Map<String, Integer> variableMap;
    private Map<Integer, Integer> gotoMap;   //use to track goto from,to values, may be used



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Runs when the activity is first created. It initializes all references to GUI components
        //Adds listeners to those components

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //add listeners for the inst buttons
        Button ifButton = (Button) findViewById(R.id.ifInstButton);
        Button letButton = (Button) findViewById(R.id.letInstButton);
        Button gotoButton = (Button) findViewById(R.id.gotoInstButton);
        Button printButton = (Button) findViewById(R.id.printInstButton);
        Button remButton = (Button) findViewById(R.id.remInstButton);
        Button gosubButton = (Button) findViewById(R.id.gosubInstButton);
        Button returnButton = (Button) findViewById(R.id.returnInstButton);


        ifButton.setOnTouchListener(instViewListener);
        letButton.setOnTouchListener(instViewListener);
        gotoButton.setOnTouchListener(instViewListener);
        printButton.setOnTouchListener(instViewListener);
        remButton.setOnTouchListener(instViewListener);
        gosubButton.setOnTouchListener(instViewListener);
        returnButton.setOnTouchListener(instViewListener);


        instList = (ListView) findViewById(R.id.instListView);
        consoleTextView = (TextView) findViewById(R.id.consoleTextView);
        consoleTextView.setMovementMethod(new ScrollingMovementMethod());  //add scroll function
        variableMap = new HashMap<>();
        gotoMap = new HashMap<>();


        instlines = new ArrayList<Instructions>() {};  //initialize first, CANNOT BE NULL, else listadapter won't work

        instlines.add(new Instructions("DUMMY"));   //dummy code, need this

        listAdapter = new DataArrayAdapter(this, instlines, variableMap);
        instList.setAdapter(listAdapter);
        instList.setOnDragListener(MyDragListener);


        instList.setOnItemLongClickListener(myLongClickListener);
        instList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        instList.setMultiChoiceModeListener(myMultiChoiceListener);


        instList.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL); //scrolls to the bottom auto



        ActionBar actionbar = getSupportActionBar();
        actionbar.setTitle("ScratchBasic Interpreter");  //check again
        actionbar.setLogo(R.mipmap.ic_launcher_new);
        actionbar.setDisplayUseLogoEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);

    }


    @Override
    protected void onResume() {
        super.onResume();

        //changedInstructions();
        listAdapter.notifyDataSetChanged();  //update the listview

    }


    @Override // create the Activity's menu from a menu resource XML file
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.interpreter_menu, menu);

        return true;
    }

    @Override   // handle choices from options menu
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            //add cases for each menu item

            case R.id.runBASIC:
                //put code here

                // instantiate a new anonymous runInterpreter object (it's an AsyncTask) and execute it
                // execute's doco indicates a variable number of parameters can be passed (here we pass none by casting null into an acceptable type)
                // btw these parameters turn up in the doInBackground method of the new task

                consoleTextView.setText("");
                instList.requestFocus();  //change focus from a selected edittext (if got) so that it can be updated
                new runInterpreter().execute((Object []) null);

                return true;
            case R.id.resetBASIC:
                resetInterpreter();
                break;
            case R.id.helpBASIC:

                // create an Intent to launch the HelpActivity
                Intent helpActivity = new Intent(MainActivity.this, HelpActivity.class);

                startActivity(helpActivity);
                break;

        }
        return true;
    }

    // 3 generic types required by any AsyncTask
    // First is the type of the data passed to the AsyncTask (not used here)
    //  this is the type of the calling execute method's actual parameter and the doInBackGround formal parameter, the result of the former is passed to the latter
    // Second is the type of progress units published during the doInBackground computation
    // Third is the type of the AsyncTask's result
    //   this is the type of the return value of doInBackground (String in this case)

    // onPostExecute method executes in GUI thread after doInBackground completes execution outside the GUI thread
    // which allows the calling Activity to safely use the AsyncTask's results
    private class runInterpreter extends AsyncTask<Object, Object, String>{
        /*
         * Runs the interpreter class which will iterate through each instruction object in a separate
          * thread and provides the output into the consoleTextView. This prevents the GUI thread from being dependent
          * on the execution of the interpreter
         */

        @Override  // executes in a separate thread
        protected String doInBackground(Object... params) {

            Interpreter it = new Interpreter();
            it.run(variableMap, instlines);
            return it.getOutputText();
        }

        @Override  // executes in the GUI thread after doInBackground completes and is passed that methods return value (String in this case)
        protected void onPostExecute(String output) {
            consoleTextView.setText(output);  //set the output from the interpreter to the textview

        }



    }

    private void resetInterpreter() {
        /*
         * Resets the listview and all the instructions lines when it is called. An alertdialog is called
         * to confirm the action as it involves deleting lines
         */

        // create a new AlertDialog Builder
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle(R.string.resetInterperterTitle);
        builder.setMessage(R.string.resetInterpreterMessage);

        builder.setPositiveButton(R.string.button_delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                instlines = new ArrayList<Instructions>() {};  //initialize first, CANNOT BE NULL, else listadapter won't work
                instlines.add(new Instructions("DUMMY"));   //dummy code, need this

                variableMap = new HashMap<>();
                gotoMap = new HashMap<>();

                listAdapter = new DataArrayAdapter(MainActivity.this, instlines, variableMap);
                instList.setAdapter(listAdapter);  //need to recall a new adapter with new instlines

                listAdapter.notifyDataSetChanged();
                consoleTextView.setText("");

            }
        });

        builder.setNegativeButton(R.string.button_cancel, null);
        builder.show();

    }



    public View.OnTouchListener instViewListener = new View.OnTouchListener() {
        /*
         * Sets up the drag listener for the instruction buttons to start drag
         */
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            //when user touches the rectangle box, add drag functionality
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);  //create shadow of the box
                v.startDrag(data, shadowBuilder, v, 0);
                v.setVisibility(View.VISIBLE);

                return true;
            } else {
                return false;
            }

        }
    };


    public View.OnDragListener MyDragListener =
            new View.OnDragListener() {
        /*
         * Sets up the drag and drop function
         */
        @Override
        public boolean onDrag(View v, DragEvent event) {

            View copyview = null;   //rectangle box to be added into...
            LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            //changedInstructions();


            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    return true;  //needs to return true so that action.drop works
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;

                case DragEvent.ACTION_DRAG_EXITED:
                    break;

                case DragEvent.ACTION_DROP:
                    View view = (View) event.getLocalState();   //get the box that was just dragged

                    int viewId = view.getId();   //get its unique id

                    switch (viewId) {
                        case R.id.ifInstButton:
                            If iF = new If("IF");
                            instlines.add(iF); //adds instruction to arraylist
                            break;

                        case R.id.letInstButton:
                            Let let = new Let("LET");
                            instlines.add(let); //adds instruction to arraylist
                            break;

                        case R.id.gotoInstButton:
                            Goto goTO = new Goto("GOTO");
                            instlines.add(goTO); //adds instruction to arraylist
                            break;

                        case R.id.printInstButton:
                            Print print = new Print("PRINT");
                            instlines.add(print); //adds instruction to arraylist
                            break;

                        case R.id.remInstButton:
                            Rem rem = new Rem("REM");
                            instlines.add(rem); //adds instruction to arraylist

                            break;

                        case R.id.gosubInstButton:

                            GoSub gosub = new GoSub("GOSUB");
                            instlines.add(gosub);

                            break;

                        case R.id.returnInstButton:

                            Return returnI = new Return("RETURN");
                            instlines.add(returnI);

                            break;
                    }

                    // Invalidates the view to force a redraw
                    //v.invalidate();

                    // Returns true. DragEvent.getResult() will return true.
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    break;
                default:
                    break;

            }

            listAdapter.notifyDataSetChanged();   //update the listview
            //instList.setOnItemLongClickListener(myLongClickListener);


            return true;
        }
    };


    public AdapterView.OnItemLongClickListener myLongClickListener = new AdapterView.OnItemLongClickListener() {
        /*
         *used for selection of inst. to delete them, not done yet
         */
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

            instList.setItemChecked(position, !listAdapter.isPositionChecked(position));

            return true;
        }
    };

    //
    public AbsListView.MultiChoiceModeListener myMultiChoiceListener = new AbsListView.MultiChoiceModeListener() {
        /*
         * allows multiple selection of lines
         */
        private int nr = 0;

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            if (checked) {
                nr++;
                if (position != 0) {
                    listAdapter.setNewSelection(position, checked);
                }

            } else {
                nr--;
                listAdapter.removeSelection(position);
            }
            mode.setTitle(nr + " selected");
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            nr = 0;
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.context_main, menu);  //inflates the contextual action bar
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {

                case R.id.menu_delete:

                    Collection<Integer> positions = listAdapter.getCurrentCheckedPosition();
                    //initialize a treeset to reorder the integers in the set, descending order
                    Set<Integer> sorted = new TreeSet<Integer>(new Comparator<Integer>() {
                        @Override  //sort the set of integers in desending order
                        public int compare(Integer o1, Integer o2) {
                            return o2.compareTo(o1);
                        }
                    });
                    sorted.addAll(positions);
                    for (Integer pos: sorted) {  //need to delete backwards
                        if (pos == 0) {
                            continue;
                        }
                        instlines.remove(instlines.get(pos));
                    }
                    listAdapter.notifyDataSetChanged();   //update the listview
                    makeSomeToast(nr);
                    nr = 0;
                    listAdapter.clearSelection();
                    mode.finish();
                    return true;
                default:
                    return false;

            }

        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            listAdapter.clearSelection();
        }

    };


    private void makeSomeToast(int num){

        // display a message indicating that the lines were deleted
        Toast message = Toast.makeText(this, Integer.toString(num) + " " + "lines have been deleted", Toast.LENGTH_SHORT);
        // gravity centres top left hand of message so offsets required
        message.setGravity(Gravity.CENTER, message.getXOffset() / 2, message.getYOffset() / 2);
        message.show(); // display the Toast

    }


}
