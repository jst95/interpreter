package au.edu.monash.fit3140.asgn3.android_interpreter;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/*
 * A class that subclass the actual AutoCompleteTextView so that drop down menu is initiated without
 * any character input.
 */
public class InputAutoComplete extends AutoCompleteTextView {


    public InputAutoComplete(Context context) {
        super(context);
    }

    public InputAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InputAutoComplete(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,
                                  Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        //change the character theshold to zero
        if (focused && getAdapter() != null) {
            performFiltering(getText(), 0);
            showDropDown();
        }
    }
}
