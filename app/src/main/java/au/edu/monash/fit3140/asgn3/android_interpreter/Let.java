package au.edu.monash.fit3140.asgn3.android_interpreter;

/**
 * A class that represents the LET statement
 */
public class Let extends Instructions {

    private String term1;
    private String term2;
    private String op;
    private String variable;

    public Let(String com) {
        super(com);
    }

    @Override
    public String getCommand() {
        return super.getCommand();
    }

    public String getVariable() {
        return variable;
    }

    public String getTerm1() {
        return term1;
    }

    public String getTerm2() {
        return term2;
    }

    public String getOp() {
        return op;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public void setTerm1(String term1) {
        this.term1 = term1;
    }

    public void setTerm2(String term2) {
        this.term2 = term2;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String toString() {
        String line = super.getCommand() + " " + variable +  " = " + term1 + " " + op + " " + term2;
        return line;
    }
}
