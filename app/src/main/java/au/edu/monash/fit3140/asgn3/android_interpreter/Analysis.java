package au.edu.monash.fit3140.asgn3.android_interpreter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/*
 * This class takes in an instruction object, analyses its content and gives the appropriate output
 */

public class Analysis {

	String command;
	private int linenum;
	ArrayList <Object> expressionList = new ArrayList<>();
	private Map<String,Integer> variableMap;
	private Instructions inst;
	private String outputText;
	private String errorMessage;
	private int nextpos = 0;
	
	public Analysis(Instructions i, Map<String,Integer> varMap, int num) {
		/*
		 * Initializes variables in the class, gets the command of the instruction and executes the
		 * appropriate function
		 */

		inst = i;
		outputText = "";
		errorMessage = "";
		variableMap = varMap;  //a map of variables and their values
		linenum = num + 1;  //increment automatically


		
		//command = inst.getCommand();
		//System.out.println(command); //this is just for testing purposes, same like other random prints

		if (inst instanceof If) {
			If v = (If) inst;
			commandIf(v);
			
		} else if (inst instanceof Let) {
			Let v = (Let) inst;
			commandLet(v);
			
		} else if (inst instanceof Goto) {
			Goto v = (Goto) inst;
			commandGoto(v);
			
		} else if (inst instanceof Print) {
			Print v = (Print) inst;
			commandPrint(v);
			
		} else if (inst instanceof GoSub){
			GoSub v = (GoSub) inst;
			commandGoSub(v);

		} else if (inst instanceof Return) {
			Return v = (Return) inst;
			commandReturn(v);

		} else {
			//rem function....
		}
	}
	
	public Analysis() {

	}

	public void commandIf(If inst) {
		/*
		 * Runs if the statement is a IF/GOTO
		 */
		

			
		Expression exp = null;
		boolean j = false;
		Object t1  = inst.getTerm1();
		Object term2 = inst.getOp();
		Object t3 = inst.getTerm2();
		Object term1;
		Object term3;

		try {
			term1 = Integer.valueOf((String) t1);

		} catch (NumberFormatException e) {
			term1 = (String) t1;
		}

		try {
			term3 = Integer.valueOf((String) t3);
		} catch (NumberFormatException e) {
			term3 = (String) t3;
		}

		if ((is_value(term1)) && (is_operator(term2)) && (is_value(term3))) {
			if ((term1 instanceof Integer) && ((term3) instanceof Integer)) {
				//eg. 6 * 9
				exp = new Expression((Integer) term1, (Integer) term3);
			} else if (((term1) instanceof String) && ((term3) instanceof Integer)) {
				//eg. y - 7

				if (variableMap.containsKey(term1.toString())) {  //checks if variable exists
					exp = new Expression(variableMap.get(term1.toString()), (Integer) term3);
				} else {
					setError("Error: Variable " + term1.toString() + " does not exist\n");
					return;
				}

			} else if (((term1) instanceof Integer) && ((term3) instanceof String)) {
				// eg. 4 + X

				if (variableMap.containsKey(term3.toString())) {  //checks if variable exists
					exp = new Expression((Integer) term1, variableMap.get(term3.toString()));
				} else {
					setError("Error: Variable " + term3.toString() + " does not exist\n");
					return;
				}

			} else {
				//eg. y > z
				int lv = 0;
				int rv = 0;

				if (variableMap.containsKey(term1.toString())) {  //checks if variable exists
					lv = variableMap.get(term1.toString());
				} else {
					setError("Error: Variable " + term1.toString() + " does not exist\n");
					return;
				}


				if (variableMap.containsKey(term3.toString())) {  //checks if variable exists
					rv = variableMap.get(term3.toString());
				} else {
					setError("Error: Variable " + term1.toString() + " does not exist\n");
					return;
				}

				exp = new Expression(lv, rv);
			}

			if (term2.toString().equals("==")) {
				j = exp.equalOp();
			} else if (term2.toString().equals(">")) {
				j = exp.moreOp();
			} else if (term2.toString().equals("+")) {
				setError("Error: Operator " + term2.toString() + " not valid in this statement\n");
			} else if (term2.toString().equals("-")) {
				setError("Error: Operator " + term2.toString() + " not valid in this statement\n");
			} else if (term2.toString().equals("*")) {
				setError("Error: Operator " + term2.toString() + " not valid in this statement\n");
			} else if (term2.toString().equals("/")) {
				setError("Error: Operator " + term2.toString() + " not valid in this statement\n");
			} else if (term2.toString().equals("%")) {
				setError("Error: Operator " + term2.toString() + " not valid in this statement\n");
			} else {
				setError("Error: Operator " + term2.toString() + " not valid\n");
			}
			//no +,- yet ...

		}

		if (j) {
			commandIfGoto(inst);
		}
	}

	
	public void commandIfGoto(If inst) {
		/*
		* Runs if the condition of the expression is met
		 */
		//consider check if given number more than number of lines

		String gotoNum = inst.getGotonum();
		if (is_value(gotoNum)) {
			
			if (isInteger(gotoNum)) {  //check this

				linenum = Integer.parseInt(gotoNum);  //set the line num to execute as you wish

			} else {
				if (variableMap.containsKey(gotoNum)) {  //checks if variable exists
					linenum = variableMap.get(gotoNum);
				} else {
					setError("Error: Variable " + gotoNum + " does not exist\n");

				}
			}
		}
	}
	
	
	public void commandLet(Let inst) {
		/*
		 * Run if the instruction is a LET statement
		 */

		Expression exp = null;
		Object term1 = inst.getVariable();
		//Object term2 = expressionList.get(1);
		Object t3 = inst.getTerm1();
		Object term4 = inst.getOp();
		Object t5 = inst.getTerm2();
		Object term3;
		Object term5;

		try {
			term3 = Integer.valueOf((String) t3);

		} catch (NumberFormatException e) {
			term3 = (String) t3;
		}

		try {
			term5 = Integer.valueOf((String) t5);
		} catch (NumberFormatException e) {
			term5 = (String) t5;
		}


		if ((term3 instanceof Integer) && (term5 instanceof Integer)) {
			//eg. 5 + 6
			exp = new Expression((Integer) term3, (Integer) term5);

		} else if ((term3 instanceof String) && (term5 instanceof Integer)) {
			//eg. X - 9

			if (variableMap.containsKey(term3.toString())) {  //checks if variable exists
				exp = new Expression(variableMap.get(term3.toString()), (Integer) term5);
			} else {
				setError("Error: Variable " + term3.toString() + " does not exist\n");
				return;
			}

		} else if ((term3 instanceof Integer) && (term5 instanceof String)) {
			//eg. 11 * T

			if (variableMap.containsKey(term5.toString())) {  //checks if variable exists
				exp = new Expression((Integer) term3, variableMap.get(term5.toString()));
			} else {
				setError("Error: Variable " + term5.toString() + " does not exist\n");
				return;
			}

		} else if ((term3 instanceof String) && (term5 instanceof String)) {
			//eg. X > Y

			int lv = 0;
			int rv = 0;


			if (variableMap.containsKey(term3.toString())) {  //checks if variable exists
				lv = variableMap.get(term3.toString());
			} else {
				setError("Error: Variable " + term3.toString() + " does not exist\n");
				return;
			}

			if (variableMap.containsKey(term5.toString())) {  //checks if variable exists
				rv = variableMap.get(term5.toString());
			} else {
				setError("Error: Variable " + term5.toString() + " does not exist\n");
				return;
			}

			exp = new Expression(lv, rv);

		} else {
			setError("Error: Your input in the IF statement is not valid\n");
			return;
		}

		int result = 0;

		if (term4.toString().equals("+")) {

			result = exp.plusOp();
		}
		 else if (term4.toString().equals("-")) {
			result = exp.minusOp();
		}  else if (term4.toString().equals("*")) {
			result = exp.multOp();

		} else if (term4.toString().equals("/")) {
			result = exp.divOp();

		} else if (term4.toString().equals("%")) {
			result = exp.modOp();
		}

	if (variableMap.containsKey(term1.toString())) {
			variableMap.put(term1.toString(),result);
		} else {  //may be redundant
			if (!term1.toString().equals("")){
				variableMap.put(term1.toString(),result);
			} else {
				setError("Error: There is no string variable in LET statement\n");

			}

		}




	}
	
	public void commandGoto(Goto inst) {
		//consider check if given number more than number of lines

		Object gotoNum = inst.getLinenum();

		System.out.println(inst.getLinenum());  //testing

		if (is_value(gotoNum)) {

			if (isInteger(gotoNum.toString())) {  //check this


				linenum = Integer.parseInt(gotoNum.toString());  //set the line num to execute as you wish

			} else {
				if (variableMap.containsKey(gotoNum.toString())) {
					linenum = variableMap.get(gotoNum.toString());
				} else {
					setError("Error: Variable " + gotoNum.toString() + " does not exist\n");

				}
			}
		}
	}
	
	public void commandPrint(Print inst) {
		/*
		 * Runs if the statement is a PRINT
		 */
		//can't print string values yet unless it is a variable

		//System.out.println(expressionList.get(0));  //testing
		String printL = inst.getValue();
		if (isInteger(printL)) {
			System.out.println(printL);
			int value = Integer.valueOf(printL);

			String line = String.valueOf(Integer.toString(value)) + "\n";
			outputText = outputText + line;   //this may be redundant
		} else {

			if (variableMap.containsKey(printL)) {
				//System.out.println(variableDict.get(i).get(1));
				String line = String.valueOf(variableMap.get(printL)) + "\n";  //change this later
				outputText = outputText + line;   //this may be redundant


			} else {
				setError("Error: Variable " + printL + " does not exist\n");

			}

		}
			
	}

	public void commandGoSub(GoSub inst) {


		String gotoNum = inst.getLinenum();
		//String nextp = inst.getNextpos();

		System.out.println(inst.getLinenum());  //testing

		nextpos = linenum;

		if (is_value(gotoNum)) {

			linenum = Integer.parseInt(gotoNum);  //set the line num to execute as you wish
		}


	}

	public void commandReturn(Return inst) {

		outputText = "RETURN";

	}

	public String getOutput() {
		/*
		 * Retrieves output from the above calculation
		 */
		if (errorMessage.equals("")) {  //checks if any error is detected
			return outputText;
		} else {
			return errorMessage;
		}

	}

	public int getLineNum() {
		//returns the line number (incremented or set by GOTO) to the interpreter file
		return linenum;
	}

	public int getNextpos() {
		return nextpos;
	}

	private boolean is_value(Object o) {
		//checks if the term given is a string or a
		
		if (o instanceof String || o instanceof Integer) {
			
			return true;
		} else {
			return false;
		}
	}
	
	private boolean is_operator(Object o) {
		//checks if the term given is a valid operator
		String op = o.toString();
		if ((op.equals("+")) || (op.equals("-")) || (op.equals("==")) || (op.equals(">"))) {
			return true;
		} else {
			return false;
		}
		
	}

	public static boolean isInteger(String s) {
		//checks if the parameter is actually an integer
		try {
			Integer.parseInt(s);  //tries to convert the string value into integer
		} catch(NumberFormatException e) {
			return false;
		} catch(NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

	public void setError(String l) {
		//sets the error message that is passed through
		errorMessage = "Line " + Integer.toString(linenum - 1) + " " + l;
	}
}
