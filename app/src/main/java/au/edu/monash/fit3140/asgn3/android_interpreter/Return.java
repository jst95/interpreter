package au.edu.monash.fit3140.asgn3.android_interpreter;

/**
 * A class that represents the RETURN statement
 */
public class Return extends Instructions {

    public Return(String com) {
        super(com);
    }

    public String toString(){
        return super.getCommand();
    }

}
