package au.edu.monash.fit3140.asgn3.android_interpreter;

/**
 * A class that represents the IF statement
 */
public class If extends Instructions {

    private String term1;
    private String term2;
    private String op;
    private String gotonum; //use for if/goto condition only

    public If(String com) {
        super(com);


    }

    @Override
    public String getCommand() {
        //Retrieves the command instruction
        return super.getCommand();
    }

    public String getTerm1() {
        return term1;
    }

    public String getTerm2() {
        return term2;
    }

    public String getOp() {
        return op;
    }

    public String getGotonum() {
        return gotonum;
    }

    public void setTerm1(String term1) {
        this.term1 = term1;
    }

    public void setTerm2(String term2) {
        this.term2 = term2;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public void setGotonum(String gotonum) {
        this.gotonum = gotonum;
    }

    public String toString() {
        String line = super.getCommand() + " " + term1 + " " + op + " " + term2  + " GOTO " + gotonum;
        return line;
    }
}


