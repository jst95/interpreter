package au.edu.monash.fit3140.asgn3.android_interpreter;

public class Instructions {
    /*
     * This class defines an instruction object that contains all the necessary values which can be
     * set and retrieved
     */

    private String command;



    public Instructions(String com) {
        //Initializes the instruction class with the command and its line
        command = com;

    }

    public String getCommand() {
        //Retrieves the command instruction
        return command;
    }




}
