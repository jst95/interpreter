package au.edu.monash.fit3140.asgn3.android_interpreter;

/*
 * A class that represents a view which is display ina spinner's drop down list
 * Used to represent a statement's line number and its contents
 */
public class GotoObj {

    private String num;
    private Instructions inst;
    private String line;

    public GotoObj(String n, Instructions ins) {
        this.num = n;
        this.inst = ins;

        if (inst instanceof If) {
            If i = (If) inst;
            line = i.toString();
        } else if (inst instanceof Let) {
            Let i = (Let) inst;
            line = i.toString();
        } else if (inst instanceof Print) {
            Print i = (Print) inst;
            line = i.toString();
        } else if (inst instanceof Rem) {
            Rem i = (Rem) inst;
            line = i.toString();
        } else if (inst instanceof GoSub) {
            GoSub i = (GoSub) inst;
            line = i.toString();
        } else if (inst instanceof Return) {
                Return i = (Return) inst;
            line = i.toString();

        } else {
            line = "";
        }
    }

    public String getNum() {
        return num;
    }

    public String getLine() {
        return line;
    }
}
