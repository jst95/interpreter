package au.edu.monash.fit3140.asgn3.android_interpreter;


import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/*
 * This interpreter class takes all the instruction lines and iterates through each one of them
 */

public class Interpreter {

	private ArrayList<Instructions> instlines;
	protected Map<String, Integer> variableMap;
	private Stack<Integer> subStack;

	private int lineNum = 1;  //not 0 as it is a dummy inst
	protected String outputText;

	
	public Interpreter() {
		//Initializes the class by setting initial values for the class variables

		variableMap = new HashMap<>();
		instlines = new ArrayList<>();
		outputText = "";
		subStack = new Stack<>();

		
	}

	
	public void run(Map<String,Integer> varMap, ArrayList<Instructions> lines) {
		/*
		 * Called by MainActivity, it goes through each instructions and passes it into an Analysis
		 * class. It then later retrieves the output from the class and a given linenum to execute
		 * next
		 */
		
		instlines = lines; //get the lines
		variableMap = varMap;

		int numLines = instlines.size();  //get total num. of lines

		while (lineNum < numLines) {
			Instructions instLine = instlines.get(lineNum);
			Analysis al = new Analysis(instLine, variableMap,lineNum);  //may need to pass variableMap


			if (al.getNextpos() != 0) {
				subStack.push(al.getNextpos());
			}
			try {
				if (al.getOutput().equals("RETURN")) {
					lineNum = subStack.pop();
				} else {
					outputText = outputText + al.getOutput(); //add output from Analysis class to main output text
				}
			} catch (EmptyStackException e) {
				outputText = outputText + "Line " + Integer.toString(lineNum) + " Return null call";
			}
			if (al.getLineNum() < numLines) {
				lineNum = al.getLineNum();
			} else {
				//outputText = outputText + "Error: GOTO value does not exist\n";
				break;
			}



		}
		
	}


	public String getOutputText() {
		//Passes the text that contains the output from the instructions lines to MainActivity
		return outputText;
	}
}
