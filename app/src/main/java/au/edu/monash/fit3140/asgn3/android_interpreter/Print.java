package au.edu.monash.fit3140.asgn3.android_interpreter;

/**
 * A class that represents the PRINT statement
 */
public class Print extends Instructions {

    private String value;

    public Print(String com) {
        super(com);
    }

    @Override
    public String getCommand() {
        return super.getCommand();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString() {
        String line = super.getCommand() + " " + value;
        return line;
    }
}
