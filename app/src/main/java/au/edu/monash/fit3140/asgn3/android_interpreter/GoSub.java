package au.edu.monash.fit3140.asgn3.android_interpreter;

/**
 * A class that represents the GOSUB statement
 */
public class GoSub extends Instructions {

    private String linenum;
    private String nextpos;

    public GoSub(String com) {
        super(com);
    }

    @Override
    public String getCommand() {
        return super.getCommand();
    }

    public String getLinenum() {
        return linenum;
    }

    public String getNextpos() {
        return nextpos;
    }

    public void setLinenum(String linenum) {
        this.linenum = linenum;
    }

    public void setNextpos(String nextpos) {
        this.nextpos = nextpos;
    }

    public String toString() {
        String line = super.getCommand() + " " + linenum;
        return line;
    }

    
}
