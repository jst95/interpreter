package au.edu.monash.fit3140.asgn3.android_interpreter;

/**
 * A class that represents the GOTO statement
 */
public class Goto extends Instructions {

    private String linenum;

    public Goto(String com) {
        super(com);
    }

    @Override
    public String getCommand() {
        return super.getCommand();
    }

    public String getLinenum() {
        return linenum;
    }

    public void setLinenum(String linenum) {
        this.linenum = linenum;
    }

    public String toString() {
        String line = super.getCommand() + " " + linenum;
        return line;
    }
}
