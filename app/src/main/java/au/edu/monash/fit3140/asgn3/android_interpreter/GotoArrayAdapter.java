package au.edu.monash.fit3140.asgn3.android_interpreter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/*
 * A class that subclasses the ArrayAdapter which allows for custom views in the spinner's
 * drop down list
 */
public class GotoArrayAdapter extends ArrayAdapter<GotoObj> {

    private List<GotoObj> gotoObjList;

    public GotoArrayAdapter(Context context,  int textViewResourceId, List<GotoObj> objects) {
        super(context, textViewResourceId, objects);
        gotoObjList = objects;
    }

    @Override
    public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
        return getCustomView(position, cnvtView, prnt); }   //display everything in the dropdown

    @Override
    public View getView(int pos, View cnvtView, ViewGroup prnt) {

        ViewGroup rowGrp = (ViewGroup) getCustomView(pos, cnvtView, prnt);
        TextView numTxt = (TextView) rowGrp.getChildAt(0);   //display the number only in the spinner box

        return numTxt;
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View mySpinner = inflater.inflate(R.layout.custom_spinner, parent, false);

        GotoObj gObj = gotoObjList.get(position);

        TextView num_text = (TextView) mySpinner .findViewById(R.id.lineSpinnerTextView);
        num_text.setText(gObj.getNum());
        TextView inst_text = (TextView) mySpinner .findViewById(R.id.instSpinnerTextView);
        inst_text.setText(gObj.getLine());

        return mySpinner; }






}
