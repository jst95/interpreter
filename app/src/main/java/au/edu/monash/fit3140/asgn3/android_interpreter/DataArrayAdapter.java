package au.edu.monash.fit3140.asgn3.android_interpreter;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ClipData;
import android.content.Context;


import android.text.Html;
import android.text.Spanned;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/*
 * This class acts as an adapter for the ListView in the GUI. It controls the display of instruction statements by
 * dynamically generating each item in the list with an associated view
*/
public class DataArrayAdapter extends ArrayAdapter<Instructions> {

    public ArrayList<Instructions> instructions;   //may use map to store commands and expressions separate
    private final Activity context;
    private Map<String, Integer> variableMap;
    private HashMap<Integer,Boolean> mSelection =  new HashMap<Integer, Boolean>();
    private ArrayList<String> variables = new ArrayList<>();
    private ArrayList<GotoObj> gotoList = new ArrayList<>();
    private ArrayList<String> operators = new ArrayList<>(Arrays.asList("==","+","-","*","/",">","%"));
    private GotoArrayAdapter spinnerGotoAdapter;
    private ArrayAdapter<CharSequence> spinnerOpAdapter;
    private ArrayAdapter<String> varACAdapter;
    private HashMap<Integer,Integer> gotoMap = new HashMap<>();
    //use a hashmap to store goto from,to values, put in mainactivity???




    public DataArrayAdapter(Activity context, ArrayList<Instructions> list, Map<String, Integer> variableMap) {
        //sets the values passed through, including the list of instructions
        super(context, R.layout.instruction_layout ,list);  //not sure
        this.instructions = list;
        this.context = context;
        this.variableMap = variableMap;

        //variables.add("Custom");  //to add new variable or integer, refer at pos 1
        for (String key: variableMap.keySet()) {
            variables.add(key);
        }

        updateGotoList();

        spinnerGotoAdapter = new GotoArrayAdapter(context,R.layout.custom_spinner,gotoList);
        spinnerGotoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        varACAdapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1, variables);

        spinnerOpAdapter = ArrayAdapter.createFromResource(context,R.array.operators, android.R.layout.simple_spinner_item);
        spinnerOpAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);



    }

    //following 6 functions are used in selection and deletions of lines
    @Override
    public void notifyDataSetChanged() {
        updateGotoList();
        //spinnerGotoAdapter.notifyDataSetChanged();
        super.notifyDataSetChanged();


    }

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        updateGotoList();
        notifyDataSetChanged();
    }

    public void clearSelection() {


        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        /*
         * Retrieves the view to be displayed in listview based on the position given
         */

        final LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        updateGotoList();

        ViewGroup rowView = (ViewGroup) inflater.inflate(R.layout.instruction_layout,parent, false);

        if (mSelection.get(position) != null) {

            rowView.setBackgroundColor(context.getResources().getColor(android.R.color.holo_blue_light));// this is a selected position so make it red
        }

        TextView numberView  = (TextView) rowView.findViewById(R.id.numberTextView);
        //TextView arrowView = (TextView) rowView.findViewById(R.id.arrowTextView);  //used for goto

        final int pos= position;
        //rowView.setTag(pos);
        numberView.setText(Integer.toString(pos));  //display line number





        //get value/command from array....
        Instructions s = instructions.get(pos);

        String command = s.getCommand();

        //change the code in the conditions for spinner

        switch (command) {
            case "IF": {  //each case have unique way of extracting, storing lines
                View instView = inflater.inflate(R.layout.if_inst_layout, null);
                //EditText expressEdit = (EditText) instView.findViewById(R.id.expressionIfEditTextView);

                //force edittext to show numeric keyboard, do this....

                final Spinner gotoIfSpinner = (Spinner) instView.findViewById(R.id.gotoIfSpinner);
                gotoIfSpinner.setAdapter(spinnerGotoAdapter);
                gotoIfSpinner.setFocusable(true);
                gotoIfSpinner.setPrompt("Choose a line:");

                final InputAutoComplete term1AC = (InputAutoComplete) instView.findViewById(R.id.termOneIfAC);
                final InputAutoComplete term2AC = (InputAutoComplete) instView.findViewById(R.id.termTwoIfAC);
                term1AC.setAdapter(varACAdapter);
                term2AC.setAdapter(varACAdapter);


                final Spinner opTerm = (Spinner) instView.findViewById(R.id.opIfSpinner);


                opTerm.setAdapter(spinnerOpAdapter);


                final If v = (If) s;

                final String t1 = v.getTerm1();
                String t2 = v.getTerm2();
                String op = v.getOp();

                //ArrayList<String> line = new ArrayList<String>(Arrays.asList(l.split(" ")));

                term1AC.setText(t1);
                opTerm.setSelection(operators.indexOf(op));
                term2AC.setText(t2);


                term1AC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int acposition, long id) {
                        term1AC.setText((String) parent.getItemAtPosition(acposition), false);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                term1AC.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (!hasFocus) {
                            String s1 = term1AC.getText().toString();
                            v.setTerm1(s1);

                        } else {
                            //term1AC.showDropDown();
                        }
                    }
                });

                term2AC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int acposition, long id) {
                        term2AC.setText((String) parent.getItemAtPosition(acposition), false);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                term2AC.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (!hasFocus) {
                            String s1 = term2AC.getText().toString();
                            v.setTerm2(s1);
                            varACAdapter.notifyDataSetChanged();
                        } else {
                            //term2AC.showDropDown();
                        }
                    }
                });




                opTerm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int spnposition, long id) {

                        v.setOp((String) parent.getItemAtPosition(spnposition));
                        opTerm.setSelection(spnposition);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                for (GotoObj gObj: gotoList) {
                    if (gObj.getNum().equals(v.getGotonum())) {
                        int gPos = gotoList.indexOf(gObj);
                        gotoIfSpinner.setSelection(gPos);
                    }
                }


                gotoIfSpinner.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        spinnerGotoAdapter.clear();  //reset adapter
                        spinnerGotoAdapter.addAll(updateGotoList());  //get new line numbers
                        spinnerGotoAdapter.notifyDataSetChanged();
                        gotoIfSpinner.performClick();  //show dropdown
                        return true;
                    }
                });



                gotoIfSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int spnposition, long id) {
                        GotoObj gObj = (GotoObj) parent.getItemAtPosition(spnposition);
                        v.setGotonum(gObj.getNum());
                        gotoIfSpinner.setSelection(spnposition);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });




                rowView.addView(instView);



                break;
            }
            case "LET": {
                View instView = inflater.inflate(R.layout.let_inst_layout, null);


                final EditText variableEdit = (EditText) instView.findViewById(R.id.variableLetEditTextView);
                //EditText expressEdit = (EditText) instView.findViewById(R.id.expressionLetEditTextView);

                final InputAutoComplete term1AC = (InputAutoComplete) instView.findViewById(R.id.termOneLetAC);
                final InputAutoComplete term2AC = (InputAutoComplete) instView.findViewById(R.id.termTwoLetAC);
                term1AC.setAdapter(varACAdapter);
                term2AC.setAdapter(varACAdapter);



                final Spinner opTerm = (Spinner) instView.findViewById(R.id.opLetSpinner);  //based on IF layout, change it...

                opTerm.setAdapter(spinnerOpAdapter);


                final Let v = (Let) s;

                final String t1 = v.getTerm1();
                String t2 = v.getTerm2();
                String op = v.getOp();

                //ArrayList<String> line = new ArrayList<String>(Arrays.asList(l.split(" ")));

                variableEdit.setText(v.getVariable());
                term1AC.setText(t1, false);
                opTerm.setSelection(operators.indexOf(op));
                term2AC.setText(t2, false);

                term1AC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int acposition, long id) {
                        term1AC.setText((String) parent.getItemAtPosition(acposition), false);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                term1AC.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (!hasFocus) {
                            String s1 = term1AC.getText().toString();
                            v.setTerm1(s1);

                        } else {
                            //term1AC.showDropDown();
                        }
                    }
                });

                term2AC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int acposition, long id) {
                        term2AC.setText((String) parent.getItemAtPosition(acposition), false);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                term2AC.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (!hasFocus) {
                            String s1 = term2AC.getText().toString();
                            v.setTerm2(s1);

                        } else {
                            //term2AC.showDropDown();
                        }
                    }
                });



                opTerm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int spnposition, long id) {

                        v.setOp((String) parent.getItemAtPosition(spnposition));
                        opTerm.setSelection(spnposition);
                        
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                variableEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                    /*
                     * When focus is lost save the entered value for
                     * later use
                     */
                        if (!hasFocus) {
                            //int itemIndex = v.getId();
                            String variableLine = ((EditText) view).getText().toString();
                            if (varACAdapter.getPosition(variableLine) == -1  && !variableLine.equals("")) {
                                String line = v.getVariable();

                                if (varACAdapter.getPosition(line) == -1) {
                                    varACAdapter.add(variableLine);
                                    //variables.add(variableLine);
                                } else {
                                    varACAdapter.remove(line);
                                    //variables.remove(line);
                                    varACAdapter.add(variableLine);
                                    //variables.add(variableLine);

                                }

                            }

                            v.setVariable(variableLine);
                            varACAdapter.notifyDataSetChanged();

                        }
                    }
                });


                rowView.addView(instView);

                break;
            }//Calculates the result of subtraction between two terms
            case "GOTO": {
                View instView = inflater.inflate(R.layout.goto_inst_layout, null);

                //EditText gotoEdit = (EditText) instView.findViewById(R.id.gotoEditTextView);

                final Spinner gotoSpinner = (Spinner) instView.findViewById(R.id.gotoSpinner);
                gotoSpinner.setAdapter(spinnerGotoAdapter);
                gotoSpinner.setFocusable(true);
                gotoSpinner.setPrompt("Choose a line:");

                final Goto v = (Goto) s;

                for (GotoObj gObj: gotoList) {
                    if (gObj.getNum().equals(v.getLinenum())) {
                        int gPos = gotoList.indexOf(gObj);
                        gotoSpinner.setSelection(gPos);
                    }
                }


                gotoSpinner.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        spinnerGotoAdapter.clear();  //reset adapter
                        spinnerGotoAdapter.addAll(updateGotoList());  //get new line numbers
                        spinnerGotoAdapter.notifyDataSetChanged();
                        gotoSpinner.performClick();  //show dropdown
                        return true;
                    }
                });



                gotoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int spnposition, long id) {
                        GotoObj gObj = (GotoObj) parent.getItemAtPosition(spnposition);
                        v.setLinenum(gObj.getNum());
                        gotoMap.put(pos,spnposition);
                        gotoSpinner.setSelection(spnposition);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                rowView.addView(instView);


                break;
            }
            case "PRINT": {
                View instView = inflater.inflate(R.layout.print_inst_layout, null);
                //EditText printEdit = (EditText) instView.findViewById(R.id.valuePrintEditTextView);
                final InputAutoComplete printAC = (InputAutoComplete) instView.findViewById(R.id.valuePrintAC);
                final Print v = (Print) s;

                String term = v.getValue();
                printAC.setAdapter(varACAdapter);
                printAC.setText(term, false);

                printAC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int acposition, long id) {
                        printAC.setText((String) parent.getItemAtPosition(acposition), false);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                printAC.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                        if (!hasFocus) {
                            String s1 = printAC.getText().toString();
                            v.setValue(s1);

                        } else {
                            //term1AC.showDropDown();
                        }
                    }
                });


                rowView.addView(instView);

                break;
            }
            case "REM": {
                View instView = inflater.inflate(R.layout.rem_inst_layout, null);
                EditText remEdit = (EditText) instView.findViewById(R.id.remEditTextView);
                final Rem v = (Rem) s;
                remEdit.setText(v.getComment());
                remEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean hasFocus) {
                    /*
                    * When focus is lost save the entered value for
                    * later use
                    */
                        if (!hasFocus) {
                            //int itemIndex = v.getId();
                            String printLine = ((EditText) view).getText().toString();
                            v.setComment(printLine);
                        }
                    }
                });


                rowView.addView(instView);


                break;
            }
            case "GOSUB": {
                View instView = inflater.inflate(R.layout.gosub_inst_layout, null);

                //EditText gotoEdit = (EditText) instView.findViewById(R.id.gotoEditTextView);

                final Spinner gosubSpinner = (Spinner) instView.findViewById(R.id.gosubSpinner);
                gosubSpinner.setAdapter(spinnerGotoAdapter);
                gosubSpinner.setFocusable(true);
                gosubSpinner.setPrompt("Choose a line:");

                final GoSub v = (GoSub) s;

                for (GotoObj gObj: gotoList) {
                    if (gObj.getNum().equals(v.getLinenum())) {
                        int gPos = gotoList.indexOf(gObj);
                        gosubSpinner.setSelection(gPos);
                    }
                }

                gosubSpinner.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        spinnerGotoAdapter.clear();  //reset adapter
                        spinnerGotoAdapter.addAll(updateGotoList());  //get new line numbers
                        spinnerGotoAdapter.notifyDataSetChanged();
                        gosubSpinner.performClick();  //show dropdown
                        return true;
                    }
                });



                gosubSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int spnposition, long id) {
                        GotoObj gObj = (GotoObj) parent.getItemAtPosition(spnposition);
                        v.setLinenum(gObj.getNum());
                        gosubSpinner.setSelection(spnposition);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                rowView.addView(instView);

                break;
            }
            case "RETURN": {

                View instView = inflater.inflate(R.layout.return_inst_layout,null);
                rowView.addView(instView);

                break;

            }

            case "DUMMY":
                //inflates the dummy layout, is actually the first item in the instruction arraylist

                return inflater.inflate(R.layout.dummy_inst_layout, null);
        }


        return rowView;

    }

    private List updateGotoList() {
        /*
         * Updates a list of line numbers used for GOTO operation
         */

        gotoList = new ArrayList<>();

        for (Instructions inst: instructions) {

            if (!(instructions.indexOf(inst) == 0)) {
                int pos = instructions.indexOf(inst);
                GotoObj gObj = new GotoObj(Integer.toString(pos), inst);
                gotoList.add(gObj);
            }



        }
        return gotoList;
        //spinnerGotoAdapter.notifyDataSetChanged();
    }

}
