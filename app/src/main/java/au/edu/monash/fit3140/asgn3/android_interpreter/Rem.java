package au.edu.monash.fit3140.asgn3.android_interpreter;

/**
 * A class that represents the REM statement
 */
public class Rem extends Instructions {

    private String comment;

    public Rem(String com) {
        super(com);
    }

    @Override
    public String getCommand() {
        return super.getCommand();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String toString() {
        String line = super.getCommand() + " " + comment;
        return line;
    }
}

