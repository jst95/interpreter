package au.edu.monash.fit3140.asgn3.android_interpreter;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class HelpActivity extends AppCompatActivity {
    /*
    * A class that represents the Help Activity
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setTitle("Help Guide");  //check again
        actionbar.setLogo(R.mipmap.ic_launcher_new);
        actionbar.setDisplayUseLogoEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);  //display app icon
        actionbar.setDisplayHomeAsUpEnabled(true);  //draws the back button
    }

    public boolean onOptionsItemSelected(MenuItem item){
        finish();   //exit this activity
        return true;

    }
}
