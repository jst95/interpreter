package au.edu.monash.fit3140.asgn3.android_interpreter;

public class Expression {
	/*
	 * A class that takes in two terms and calculates the result based on the function that
	  * is executed
	 */

	private int lhs;
	private int rhs;
	int result;
	
	public Expression(int l, int r) {
		// Initializes the class with the two terms passed through
		
		super();
		lhs = l;
		rhs = r;
	}
	
	public int plusOp() {
		//Calculates the result of addition between two terms
		
		result = lhs + rhs;
		return result;
	}
	
	public int minusOp() {
		//Calculates the result of subtraction between two terms
		
		result = lhs - rhs;
		return result;
	}

	public int multOp() {
		//Calculates the result of multiplication between two terms
		result = lhs * rhs;
		return result;
	}

	public int divOp() {
		//Calculates the result of division between two terms
		result = lhs / rhs;
		return result;
	}

	public int modOp() {
		//Calculates the result of modulus between two terms
		result = lhs % rhs;
		return result;
	}
	
	public boolean equalOp() {
		//Determines whether both terms are equal and returns a boolean response
		
		if (lhs == rhs) {  //checks if both terms are equal
			return true;
		} else {
			return false;
		}
	}
	
	public boolean moreOp() {
		//Determines whether a term is more than the other and returns a boolean response
		
		if (lhs > rhs) {
			return true;
		} else {
			return false;
		}
	}
}
